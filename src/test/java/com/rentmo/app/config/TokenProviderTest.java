package com.rentmo.app.config;


import com.rentmo.app.model.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Spy;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collections;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {TokenProvider.class})
public class TokenProviderTest {
    @Spy
    private TokenProvider tokenProvider;

    @Test
    public void testGetClaimsFromUserInstanceReturnsConsistentMap() {

        User user = new User();
        user.setFirstname("testuser");
        user.setLastname("testuser1");
        user.setEmail("test@user.com");
        user.setRoles(Collections.singletonList("ADMIN"));

        Map<String, Object> claims = tokenProvider.getClaimsFromUserInstance(user);
        assertNotNull(claims);

        assertNotNull(claims.get("firstname"));
        assertEquals(user.getFirstname(), claims.get("firstname"));

        assertNotNull(claims.get("roles"));
        assertEquals(user.getRoles(), claims.get("roles"));

        assertNotNull(claims.get("lastname"));
        assertEquals(user.getLastname(), claims.get("lastname"));

        assertNotNull(claims.get("email"));
        assertEquals(user.getEmail(), claims.get("email"));

    }
}
