package com.rentmo.app.repository;

import com.rentmo.app.model.User;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface UserRepository  extends CrudRepository<User,String> {
    User findByEmail(String email);

    Optional<User> findByUuid(String uuid);

    Optional<User> findById(String id);
}
