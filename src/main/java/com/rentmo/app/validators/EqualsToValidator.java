package com.rentmo.app.validators;

import com.rentmo.app.validators.interfaces.EqualsTo;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.PropertyAccessorFactory;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class EqualsToValidator implements ConstraintValidator<EqualsTo, Object> {
    private String field1;
    private String field2;

    @Override
    public void initialize(final EqualsTo annotation) {
        field1 = annotation.field1();
        field2 = annotation.field2();
    }

    @Override
    public boolean isValid(final Object value, final ConstraintValidatorContext context) {

        BeanWrapper wrapper = PropertyAccessorFactory.forBeanPropertyAccess(value);

        Object valueOfField1 = wrapper.getPropertyValue(field1);
        Object valueOfField2 = wrapper.getPropertyValue(field2);

        boolean ok = valueOfField1 != null && valueOfField2 != null && String.valueOf(valueOfField1).equals(String.valueOf(valueOfField2));
        if (!ok) {
            context.disableDefaultConstraintViolation();
            context.buildConstraintViolationWithTemplate(context.getDefaultConstraintMessageTemplate())
                    .addPropertyNode(field1)
                    .addConstraintViolation();

            return false;
        }

        return true;
    }

}
