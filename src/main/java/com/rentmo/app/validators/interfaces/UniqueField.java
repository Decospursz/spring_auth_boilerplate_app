package com.rentmo.app.validators.interfaces;

import com.rentmo.app.validators.UniqueFieldValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;


@Documented
@Constraint(validatedBy = {UniqueFieldValidator.class})
@Retention(RetentionPolicy.RUNTIME)

@Target({
        ElementType.ANNOTATION_TYPE,
        ElementType.FIELD
})
public @interface UniqueField {
    String message() default "please choose a different value. this one already exists";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    boolean isUpdate() default false;

    String columnName();

    Class<?> className();
}
