package com.rentmo.app.validators;

import com.rentmo.app.validators.interfaces.UniqueField;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class UniqueFieldValidator implements ConstraintValidator<UniqueField, Object> {
    private Class className;
    private String columnName;
    private boolean isUpdate;

    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public void initialize(final UniqueField annotation) {
        className = annotation.className();
        columnName = annotation.columnName();
        isUpdate = annotation.isUpdate();
    }

    @Override
    public boolean isValid(final Object value, final ConstraintValidatorContext context) {

        if (value == null) {
            return true;
        }

        Query query = new Query();
        query.addCriteria(Criteria.where(columnName).is(value));

        Object o = mongoTemplate.findOne(query, className);

        return o == null;
    }
}
