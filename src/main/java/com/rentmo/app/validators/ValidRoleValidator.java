package com.rentmo.app.validators;

import com.rentmo.app.validators.interfaces.ValidRole;
import org.springframework.beans.factory.annotation.Value;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Arrays;
import java.util.List;

public class ValidRoleValidator implements ConstraintValidator<ValidRole, Object> {
    @Value("${user.roles}")
    private String roles;



    @Override
    public boolean isValid(final Object value, final ConstraintValidatorContext context) {
        if (value != null) {
            List<String> selected = (List) value;
            List<String> listOfRoles = Arrays.asList(roles.split(","));
            return listOfRoles.containsAll(selected);
        }

        return false;
    }
}

