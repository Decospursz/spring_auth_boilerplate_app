package com.rentmo.app.config;


import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.google.common.annotations.VisibleForTesting;
import com.rentmo.app.model.User;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.*;
import java.util.stream.Collectors;

import static com.rentmo.app.dto.input.Constants.AUTHORITIES_KEY;

@Component
public class TokenProvider implements Serializable {
    @Value("${jwt.private.key}")
    private String jwtPrivateKeyPem;
    @Value("${jwt.public.key}")
    private String jwtPublicKey;
    @Value("${jwt.key.issuer}")
    private String jwtKeyIssuer;

    private String userId;
    private String email;
    private String firstname;
    private String lastname;
    private List<String> roles;

    void setDetails(String token){
        DecodedJWT claims = JWT.decode(token);
        userId = claims.getClaim("userId").asString();
        this.email = claims.getClaim("email").asString();
        this.firstname = claims.getClaim("firstname").asString();
        this.lastname = claims.getClaim("lastname").asString();
        this.roles = claims.getClaim("roles").asList(String.class);
    }

    public String getUserId() {
        return this.userId;
    }

    public String getUserEmail() {
        return this.email;
    }

    public String getUserName() {
        return this.firstname + " " + this.lastname;
    }

    public List<String> getRoles() {
        return roles;
    }

    String getUsernameFromToken(String token) {
        return JWT.decode(token).getSubject();
    }

    private Date getExpirationDateFromToken(String token) {
        return JWT.decode(token).getExpiresAt();
    }

    private Boolean isTokenExpired(String token) {
        final Date expiration = getExpirationDateFromToken(token);
        return expiration.before(new Date());
    }

    private Algorithm getAlgorithm() throws NoSuchAlgorithmException, InvalidKeySpecException {
        jwtPrivateKeyPem = jwtPrivateKeyPem.replaceAll("\\n", "").replace("-----BEGIN PRIVATE KEY-----", "").replace("-----END PRIVATE KEY-----", "");
        jwtPublicKey = jwtPublicKey.replaceAll("\\n", "").replace("-----BEGIN PUBLIC KEY-----", "").replace("-----END PUBLIC KEY-----", "");

        KeyFactory kf = KeyFactory.getInstance("RSA");

        PKCS8EncodedKeySpec keySpecPKCS8 = new PKCS8EncodedKeySpec(Base64.getDecoder().decode(jwtPrivateKeyPem));
        RSAPrivateKey privateKey = (RSAPrivateKey) kf.generatePrivate(keySpecPKCS8);

        X509EncodedKeySpec keySpecX509 = new X509EncodedKeySpec(Base64.getDecoder().decode(jwtPublicKey));
        RSAPublicKey publicKey = (RSAPublicKey) kf.generatePublic(keySpecX509);

        return Algorithm.RSA256(publicKey, privateKey);
    }


    public String generateToken(Authentication authentication, User user) throws NoSuchAlgorithmException, InvalidKeySpecException {

        Calendar tokenExpiry = Calendar.getInstance();
        tokenExpiry.add(Calendar.HOUR, 1);

        Algorithm algorithm = getAlgorithm();
        String token = JWT.create()
                .withSubject(authentication.getName())
                .withIssuer(jwtKeyIssuer)
                .withExpiresAt(tokenExpiry.getTime())
                .withArrayClaim("roles", user.getRoles().toArray(new String[0]))
                .withClaim("firstname", user.getFirstname())
                .withClaim("lastname", user.getLastname())
                .withClaim("email", user.getEmail())
                .withClaim("userId", user.getUuid())
                .sign(algorithm);
        return token;
    }

    @VisibleForTesting
    Map<String, Object> getClaimsFromUserInstance(User user) {
        Map<String, Object> claims = new HashMap<>();
        claims.put("roles", user.getRoles());
        claims.put("firstname", user.getFirstname());
        claims.put("lastname", user.getLastname());
        claims.put("email", user.getEmail());
        claims.put("userId", user.getUuid());
        return claims;
    }

    Boolean validateToken(String token) throws InvalidKeySpecException, NoSuchAlgorithmException {
        JWTVerifier verifier = JWT.require(getAlgorithm())
                .withIssuer(jwtKeyIssuer)
                .build();
        DecodedJWT jwt = verifier.verify(token);
        final String username = getUsernameFromToken(token);
        return (jwt != null && !isTokenExpired(token));
    }

    UsernamePasswordAuthenticationToken getAuthentication(final String token, final Authentication existingAuth, final UserDetails userDetails) {

        Claim claims = JWT.decode(token).getClaim(AUTHORITIES_KEY);

        final Collection<? extends GrantedAuthority> authorities =
                Arrays.stream(claims.toString().split(","))
                        .map(SimpleGrantedAuthority::new)
                        .collect(Collectors.toList());

        return new UsernamePasswordAuthenticationToken(userDetails, "", authorities);
    }
}
