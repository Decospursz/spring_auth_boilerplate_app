package com.rentmo.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RentmoApplication {

	public static void main(String[] args) {
		SpringApplication.run(RentmoApplication.class, args);
	}

}
