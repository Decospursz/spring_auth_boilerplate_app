package com.rentmo.app.controller;

import com.rentmo.app.dto.input.LoginInputDTO;
import com.rentmo.app.dto.input.RegisterUserInputDTO;
import com.rentmo.app.dto.output.LoginResponseDTO;
import com.rentmo.app.dto.output.UserResponseDTO;
import com.rentmo.app.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;


@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/v1/auth")
public class AuthController extends Controller {
    @Autowired
    private UserService userService;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private HttpServletRequest request;

    @Autowired
    private HttpServletResponse response;

    @PostMapping("/login")
    public LoginResponseDTO login(@RequestBody LoginInputDTO loginUser) {
        LoginResponseDTO serviceResponse = userService.loginUser(loginUser, authenticationManager);
        updateHttpStatus(serviceResponse);
        return serviceResponse;
    }

    @PostMapping("/register")
    public UserResponseDTO register(@RequestBody @Valid RegisterUserInputDTO registerUserInputDto) {
        UserResponseDTO serviceResponse = userService.registerUser(registerUserInputDto);
        updateHttpStatus(serviceResponse);
        return serviceResponse;
    }
}
