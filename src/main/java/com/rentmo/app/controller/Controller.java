package com.rentmo.app.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rentmo.app.dto.output.StandardResponseDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static com.rentmo.app.dto.enums.Status.*;

public class Controller {
    private final Logger log = LoggerFactory.getLogger(Controller.class);

    @Autowired
    private HttpServletResponse response;

    @Autowired
    private HttpServletRequest request;

    <T extends StandardResponseDTO> void updateHttpStatus(T responseDTO) {
        switch (responseDTO.getStatus()) {
            case SUCCESS:
                response.setStatus(HttpStatus.OK.value());
                break;
            case INTERNAL_ERROR:
                response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
                break;
            case NOT_FOUND:
                response.setStatus(HttpStatus.NOT_FOUND.value());
                break;
            case FORBIDDEN:
                response.setStatus(HttpStatus.FORBIDDEN.value());
                break;
            default:
                response.setStatus(HttpStatus.BAD_REQUEST.value());
        }

        log.info("request to endpoint: " + request.getRequestURI());
        try {
            String response = new ObjectMapper().writeValueAsString(responseDTO);
            int limit = Math.min(response.length(), 50);

            log.info("returning: " + response);
        } catch (JsonProcessingException e) {
            log.error("could't echo response");
        }
    }
}
