package com.rentmo.app.service;

import com.rentmo.app.config.TokenProvider;
import com.rentmo.app.dto.enums.Status;
import com.rentmo.app.dto.input.LoginInputDTO;
import com.rentmo.app.dto.input.RegisterUserInputDTO;
import com.rentmo.app.dto.output.LoginResponseDTO;
import com.rentmo.app.dto.output.UserResponseDTO;
import com.rentmo.app.model.User;
import com.rentmo.app.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;


@Service(value = "userService")
public class UserService extends AbstractService implements UserDetailsService{
    private static final String SERVICE_NAME = "UserService";

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TokenProvider tokenProvider;

    @Autowired
    private Environment env;

    @Value("${user.role.admin}")
    private String adminRole;

    @Value("${user.role.user}")
    private String userRole;

    @Value("${user.roles}")
    private String roles;

    @Value("${user.roles.default}")
    private String defaultRoles;

    @Autowired
    private BCryptPasswordEncoder bcryptEncoder;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByEmail(username);
        if (user == null) {
            throw new UsernameNotFoundException("Invalid username or password.");
        }
        return new org.springframework.security.core.userdetails.User(user.getEmail(), user.getPassword(), getAuthority(user));
    }

    private Set<SimpleGrantedAuthority> getAuthority(User user) {
        Set<SimpleGrantedAuthority> authorities = new HashSet<>();
        for (String role : user.getRoles()) {
            authorities.add(new SimpleGrantedAuthority("ROLE_" + role));
        }
        return authorities;
    }

    private User findOne(String username) {
        return userRepository.findByEmail(username);
    }

    @Transactional
    public UserResponseDTO registerUser(RegisterUserInputDTO user) {
        try {
            User newUser = new User();
            newUser.setEmail(user.getEmail());
            newUser.setPassword(bcryptEncoder.encode(user.getPassword()));
            newUser.setFirstname(user.getFirstname());
            newUser.setLastname(user.getLastname());
            newUser.setRoles(Collections.singletonList(userRole));
            newUser.setCreatedDate(new Date());
            newUser.setPendingPasswordUpdate(Boolean.TRUE);
            newUser.setUuid(UUID.randomUUID().toString());

            userRepository.save(newUser);


            return new UserResponseDTO(Status.SUCCESS, newUser);
        } catch (Exception e) {
            logError(SERVICE_NAME, "registerUser", user.getEmail(), e);
            return new UserResponseDTO(Status.INTERNAL_ERROR);
        }
    }


    public LoginResponseDTO loginUser(LoginInputDTO loginUser, AuthenticationManager authenticationManager) {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginUser.getUsername(),
                        loginUser.getPassword()
                )
        );
        SecurityContextHolder.getContext().setAuthentication(authentication);

        User u = findOne(loginUser.getUsername());

        try {
            String token = tokenProvider.generateToken(authentication, u);



            return new LoginResponseDTO(Status.SUCCESS, token, u);
        } catch (Exception e) {
            logError(SERVICE_NAME, "loginUser", null, e);
            return new LoginResponseDTO(Status.INTERNAL_ERROR);
        }

    }

}
