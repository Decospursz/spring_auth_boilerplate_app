package com.rentmo.app.service;

import com.rentmo.app.config.TokenProvider;
import com.rentmo.app.dto.input.ApiFieldError;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.validation.BindingResult;

import java.util.List;

import static java.util.stream.Collectors.toList;

public class AbstractService {
    @Value("${user.role.admin}")
    private String adminRole;

    private String me;

    void logError(String serviceName, String methodName, String user, Exception ex) {
        ex.printStackTrace(); //log in some Logging UI
    }


    @Autowired
    private TokenProvider tokenProvider;

    String getUserId() {
        return tokenProvider.getUserId();
    }

    String getUserEmail() {
        return tokenProvider.getUserEmail();
    }

    String getUsername() {
        return tokenProvider.getUserName();
    }

    protected boolean isAdminUser(){
        return tokenProvider.getRoles() != null && tokenProvider.getRoles().contains(adminRole);
    }

    List<ApiFieldError> getValidationErrors(BindingResult result) {
        return result.getFieldErrors()
                .stream()
                .map(fieldError -> new ApiFieldError(
                        fieldError.getField(),
                        fieldError.getDefaultMessage(),
                        fieldError.getRejectedValue())
                )
                .collect(toList());
    }
}
