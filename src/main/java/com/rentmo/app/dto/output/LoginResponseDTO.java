package com.rentmo.app.dto.output;

import com.rentmo.app.dto.enums.Status;
import com.rentmo.app.model.User;

public class LoginResponseDTO extends StandardResponseDTO{

    private String token;
    private User user;

    public LoginResponseDTO() {
    }

    public LoginResponseDTO(Status status) {
        super(status);
    }

    public LoginResponseDTO(Status status, String token, User user) {
        super(status);
        this.token = token;
        this.user = user;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }


}
