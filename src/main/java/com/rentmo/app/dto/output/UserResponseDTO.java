package com.rentmo.app.dto.output;

import com.rentmo.app.dto.enums.Status;
import com.rentmo.app.model.User;

public class UserResponseDTO extends StandardResponseDTO  {
    private User user;

    public UserResponseDTO(Status status, User user) {
        super(status);
        this.user = user;
    }

    public UserResponseDTO(Status status) {
        this(status, null);
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
